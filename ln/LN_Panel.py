import bpy
import nodeitems_utils

from wk_logic_nodes import Utils

from .LN_Tree import LN_Tree

__all__ = ['LN_Panel']

@Utils.RegisteredClass
class LN_Panel(bpy.types.Panel):
    '''BGE Logic Nodes Panel'''
    bl_label = 'BGE Logic Nodes'
    bl_space_type = 'NODE_EDITOR'
    bl_category = 'BGE Logic Nodes'
    bl_region_type = 'TOOLS'

    @classmethod
    def poll(cls, context):
        return context.space_data.tree_type == LN_Tree.bl_idname

    def draw(self, context):
        box = self.layout.box()
