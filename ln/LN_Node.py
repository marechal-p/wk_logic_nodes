import bpy

from .LN_Tree import LN_Tree

__all__ = ['LN_Node']

class LN_Node(bpy.types.Node):
    @classmethod
    def poll(cls, node_tree):
        return node_tree.bl_idname == LN_Tree.bl_idname

    bl_idname = 'LN_Node'
    bl_label = 'Logic Node'
    bl_icon = 'LOGIC'

    def set_color(self, color):
        self.use_custom_color = True
        self.color = color

    def draw_label(self):
        return self.__class__.bl_label
