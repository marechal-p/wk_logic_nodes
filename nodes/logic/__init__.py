from wk_logic_nodes.ln.LN_NodeCategory import *

LogicCategory = LN_NodeCategory('LN_LogicCategory', 'Logic', 'The logic operators', color=(.4, .4, 0))
