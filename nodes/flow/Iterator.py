import bpy

from wk_logic_nodes.ln.LN_Node import *
from . import FlowCategory

__all__ = ['LN_IteratorOperatorNode']

@FlowCategory.addNodeItem(label='Iterator')
class LN_IteratorOperatorNode(LN_Node):
    bl_idname = 'LN_IteratorOperatorNode'
    bl_label = 'Iterator Operator'

    def init(self, context):
        self.inputs.new('LN_EventSocket', 'event')
        self.inputs.new('LN_ParameterSocket', 'iterable')
        self.outputs.new('LN_ParameterSocket', 'item[0]')
        self.set_color(FlowCategory.color)

    def draw_buttons(self, context, layout):
        if len(self.outputs) > 1 and not self.outputs[-2].is_linked:
            for socket in reversed(self.outputs[1:]):
                if not socket.is_linked:
                    self.outputs.remove(socket)
                else: break
        if self.outputs[-1].is_linked:
            self.outputs.new('LN_ParameterSocket',
                'item[{}]'.format(len(self.outputs)))
