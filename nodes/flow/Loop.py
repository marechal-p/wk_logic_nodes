import bpy

from wk_logic_nodes.ln.LN_Node import *
from . import FlowCategory

__all__ = ['LN_LoopOperatorNode']

@FlowCategory.addNodeItem(label='Loop')
class LN_LoopOperatorNode(LN_Node):
    bl_idname = 'LN_LoopOperatorNode'
    bl_label = 'Loop Operator'

    def init(self, context):
        self.inputs.new('LN_EventSocket', 'event')
        self.inputs.new('LN_ConditionSocket', 'condition')
        self.outputs.new('LN_EventSocket', 'event')
        self.set_color(FlowCategory.color)
