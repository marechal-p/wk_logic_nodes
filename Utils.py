import traceback

__all__ = [
    'RegisteredClasses',
    'RegisteredClass',

    'ftry',
]

RegisteredClasses = set()
def RegisteredClass(cls):
    '''This is a class decorator'''
    RegisteredClasses.add(cls)
    return cls

def ftry(f, *args, **kargs):
    try:
        f(*args, **kargs)
    except:
        traceback.print_exc()
