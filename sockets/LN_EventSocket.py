import bpy

from wk_logic_nodes import Utils
from .LN_InversableSocket import *

__all__ = ['LN_EventSocket']

@Utils.RegisteredClass
class LN_EventSocket(LN_InversableSocket):
    '''Represents the flow of execution'''
    bl_idname = 'LN_EventSocket'
    bl_label = 'Event Socket'

    inverted = bpy.props.BoolProperty(default=False)

    def draw_color(self, context, node):
        return (1, .5, 0, 1)
