import bpy

from .LN_SimpleSocket import *

__all__ = ['LN_InversableSocket']

class LN_InversableSocket(LN_SimpleSocket):
    '''Represents a socket that could be inverted'''
    bl_idname = 'LN_InversableSocket'
    bl_label = 'Inversable Socket'

    invert_icon = 'PANEL_CLOSE'
    inverted = bpy.props.BoolProperty(default=False)

    def draw(self, context, layout, node, text):
        text = text.title()

        if self.is_output:
            layout.label(text)
            layout.prop(self, 'inverted', icon=self.invert_icon, icon_only=True)
        else:
            layout.prop(self, 'inverted', icon=self.invert_icon, icon_only=True)
            layout.label(text)
