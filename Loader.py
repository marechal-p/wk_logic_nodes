from wk_logic_nodes import Utils
from wk_logic_nodes import Log

import importlib
import pkgutil
import types

def _load(module, v=False, recursive=True):
    if isinstance(module, str):
        module = importlib.import_module(module)
    imports = dict()

    if v: Log.d('Loaded {}'.format(module.__name__))

    if hasattr(module, '__path__') and recursive:
        for loader, name, is_pkg in pkgutil.walk_packages(module.__path__):
            subName = '{}.{}'.format(module.__name__, name)
            imports[subName] = importlib.import_module(subName)
            if v: Log.d('Loaded {} from {}'.format(subName, module.__name__))
            if is_pkg:
                imports.update(_load(subName))

    return imports

def _reload(module, v=False, recursive=True):
    if isinstance(module, str):
        module = importlib.import_module(module)
    importlib.reload(module)
    if v: Log.d('Reloaded {}'.format(module))

    if hasattr(module, '__path__') and recursive:
        for loader, name, is_pkg in pkgutil.walk_packages(module.__path__):
            subName = '{}.{}'.format(module.__name__, name)
            _reload(subName)

def load(*modules, **kargs):
    '''Loads modules recursively.'''
    for module in modules:
        Utils.ftry(_load, module, **kargs)

def reload(*modules, **kargs):
    '''Reloads modules recursively.'''
    for module in modules:
        Utils.ftry(_reload, module, **kargs)
